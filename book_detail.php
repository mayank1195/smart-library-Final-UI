<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>BOOK DETAIL</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="http://assets.bookchor.xyz/global/vendor/bootstrap-4/css/bootstrap.min.css">
    <link rel="stylesheet" href="OwlCarousel2/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="OwlCarousel2/dist/assets/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/book_detail.css">
    <link rel="stylesheet" href="assets/bookchor_icons/styles.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="http://assets.bookchor.xyz/global/css/global.css">
    <link rel="stylesheet" type="text/css" href="http://assets.bookchor.xyz/global/css/element.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
        crossorigin="anonymous">
</head>

<body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark ">

        <div class="container-fluid">
            <h6 id="brand-name" class="float-left font-weight-bold">St-Stephen School</h6>

            <ul class="nav float-right" id="nav-list">
                <li class="nav-item">
                    <a class="nav-link " href="#"><i class="fas fa-heart"></i></a>
                </li>
                <li class="nav-item" id="user-history">
                    <a class="nav-link" href="#">Issue History</a>
                </li>
                <li class="nav-item" id="user-clock">
                    <a class="nav-link" href="#"><img src="images/History_icon.svg" class="img-fluid" alt="icon"></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fas fa-user-circle"></i></a>
                </li>
            </ul>
        </div>

    </nav>

    <section>
        <div class="container">

            <div class="card test">
                <div class="card-body" style="padding:10px">

                    <div class="grid">
                        <div class="card-body">
                            <img src="images/Book Image.svg" alt="logo" class="img-fluid mx-auto d-block">

                        </div>
                        <div class="card-body">

                            <small class="text-muted float-left">Book Name</small>

                            <small class="float-right right-content">
                                <div class="grid2">
                                    <p class="font-weight-bold float-left" >145</p>
                                    <p class="text-muted float-left" >Reviews</p>

                                    <p class="font-weight-bold float-left" >(4.5)</p>
                                    <i class="fas fa-star align-center"></i>
                                </div>


                            </small>
                            <br>
                            <p class="font-weight-bold float-left" >Out of the
                                Box..</p>

                            <br>

                            <small class="text-muted float-left">Author Name</small>
                            <br>
                            <p class="font-weight-bold float-left" >BY JOHN DUE</p>
                            <br>

                            <small class="text-muted float-left">Publisher</small>
                            <br>
                            <p class="font-weight-bold float-left" >Jaico Publishing House</p>
                            <br>

                            <small class="text-muted float-left">Description</small>
                            <br>
                            <p class="float-left">Lorem ipsum dolor sit amet consectetur
                                adipisicing elit. Inventore, consequatur.</p>
                            <br>
                            <br>

                        </div>
                    </div>

                </div>

                <div class="card-body return-date">
                    <div class="grid3">

                        <div class="card-body one">
                            <button class="btn2 font-weight-bold"><a href="#">Issue Book</a></button>
                        </div>

                        <div class="card-body two">
                            <button class="btn2 font-weight-bold"><a href="#">Find Book</a></button>
                        </div>

                        <div class="card-body three">
                            <button class="btn2 font-weight-bold" type="button" data-toggle="modal" data-target="#exampleModalCenter"><a
                                    href="#">Recommend Book</a></button>

                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
                                aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title font-weight-bold" id="exampleModalLongTitle">Book</h5>
                                            
                                        </div>

                                        <div class="modal-body">
                                            <div class="form-group has-search">
                                                <!-- <span class="fa fa-search form-control-feedback"></span> -->
                                                <input type="text" class="form-control" placeholder="Search for students..">
                                            </div>
                                            <!-- <i class="fas fa-user float-left"></i> -->
                                            <p class="float-left font-weight-bold" >Mayank Haldunia</p>
                                            <i class="bc-bc-tick-black float-right"></i>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <div class="card others">
                <div class="card-body" style="padding:10px">
                    <p class="font-weight-bold float-left" >Others</p>
                    <br>
                    <div class="grid4">
                        <div class="others-content" >
                            <div class="grid7">
                                <p class="text-muted float-left" >ISBN</p>
                                <p class="font-weight-bold float-right" >#4444444444444</p>
                            </div>

                        </div>
                        <div class="others-content">
                            <div class="grid7">
                                <p class="text-muted float-left" >Edition</p>
                                
                                <p class="font-weight-bold float-right">First Edition</p>
                            </div>

                        </div>
                        <div class="others-content">
                            <div class="grid7">
                                <p class="text-muted float-left" >Cover</p>
                                <p class="font-weight-bold float-right" >Hard Cover</p>
                            </div>

                        </div>
                        <div class="others-content">
                            <div class="grid7">
                                <p class="text-muted float-left" >MRP</p>
                                <p class="font-weight-bold float-right">120/-</p>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">

            <div class="card reviews">
                <div class="card-body">
                    <p class="font-weight-bold float-left">Reviews</p>
                    <button class="btn1 float-right" type="button" data-toggle="modal" data-target="#myModal">Write
                        Review</button>

                    <div class="modal fade" id="myModal">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header" >
                                    <h5 class="modal-title">Select</h5>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <!-- Modal body -->
                                <div class="modal-body">
                                    <!-- <i class="fas fa-star align-center star" ></i>&nbsp;
                                    <i class="fas fa-star align-center star"></i>&nbsp;
                                    <i class="fas fa-star align-center star"></i>&nbsp;
                                    <i class="fas fa-star align-center star"></i>&nbsp;
                                    <i class="fas fa-star align-center star"></i> -->

                                    <fieldset class="rating">
                                        <input type="radio" id="star5" name="rating" value="5" />
                                        <label class = "full" for="star5" title="5 stars"></label>

                                        <input type="radio" id="star4half" name="rating" value="4 and a half" />
                                        <label class="half" for="star4half" title="4.5 stars"></label>

                                        <input type="radio" id="star4" name="rating" value="4" />
                                        <label class = "full" for="star4" title="4 stars"></label>

                                        <input type="radio" id="star3half" name="rating" value="3 and a half" />
                                        <label class="half" for="star3half" title="3.5 stars"></label>

                                        <input type="radio" id="star3" name="rating" value="3" />
                                        <label class = "full" for="star3" title="3 stars"></label>

                                        <input type="radio" id="star2half" name="rating" value="2 and a half" />
                                        <label class="half" for="star2half" title="2.5 stars"></label>

                                        <input type="radio" id="star2" name="rating" value="2" />
                                        <label class = "full" for="star2" title="2 stars"></label>

                                        <input type="radio" id="star1half" name="rating" value="1 and a half" />
                                        <label class="half" for="star1half" title="1.5 stars"></label>

                                        <input type="radio" id="star1" name="rating" value="1" />
                                        <label class = "full" for="star1" title="1 star"></label>

                                        <input type="radio" id="starhalf" name="rating" value="half" />
                                        <label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>

                                    </fieldset>
                                    
                                    <br><br>
                                    <input type="text" class="form-control write-review" placeholder="Write Review">
                                </div>

                                <!-- Modal footer -->
                                <div class="modal-footer">
                                    <button type="button" class="btn font-weight-bold" data-dismiss="modal">Cancel</button>
                                    <button type="button" class="btn font-weight-bold" style="background:#F2A614">Post</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-body" style="padding:10px !important">
                    <div class="grid5">

                        <div class="others-content" >
                            <div class="grid6">
                                <div>
                                    <i class="fas fa-user float-left"></i>
                                </div>
                                <div>
                                    <p class="font-weight-bold float-left" >Mayank Haldunia (5)</p>
                                    &nbsp;
                                    <i class="fas fa-star "></i>
                                    <br>
                                    <p class="text-muted float-left" >This is really nice book...</p>
                                    
                                    <small><a href="#">Read More</a></small>
                                </div>
                            </div>

                        </div>

                        <div class="others-content">
                            <div class="grid6">
                                <div>
                                    <i class="fas fa-user float-left"></i>
                                </div>
                                <div>
                                    <p class="font-weight-bold float-left">Mayank Haldunia (5)</p>
                                    &nbsp;
                                    <i class="fas fa-star "></i>
                                    <br>
                                    <p class="text-muted float-left" >This is really nice book...</p>
                                    
                                    <small><a href="#">Read More</a></small>
                                </div>
                            </div>

                        </div>

                        <div class="others-content">
                            <div class="grid6">
                                <div>
                                    <i class="fas fa-user float-left"></i>
                                </div>
                                <div>
                                    <p class="font-weight-bold float-left" >Mayank Haldunia (5)</p>
                                    &nbsp;
                                    <i class="fas fa-star "></i>
                                    <br>
                                    <p class="text-muted float-left" >This is really nice book...</p>
                                    
                                    <small><a href="#">Read More</a></small>
                                </div>
                            </div>

                        </div>

                        <div class="others-content">
                            <div class="grid6">
                                <div>
                                    <i class="fas fa-user float-left"></i>
                                </div>
                                <div>
                                    <p class="font-weight-bold float-left" >Mayank Haldunia (5)</p>
                                    &nbsp;
                                    <i class="fas fa-star"></i>
                                    <br>
                                    <p class="text-muted float-left">This is really nice book...</p>
                                    
                                    <small><a href="#">Read More</a></small>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>

        </div>
    </section>

    <section class="slider-content" >
        <div class="container">

            <div class="card books-you-might-like">
                <div class="card-body books">
                    <p class="font-weight-bold float-left" >Books You Might Like</p>
                </div>
            </div>

            <div id="owl-test" class="owl-carousel owl-theme">
                <div class="item ">
                    <img src="images/img_avatar3.png" alt="logo" class="img-fluid ">
                    <p class="font-weight-bold book-title" >Out
                        Of the Box</p>
                    <p class="text-muted " >by John Doe</p>
                    <div class="stars ">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>&nbsp;
                        <small class="font-weight-bold">(5)</small>
                    </div>
                </div>

                <div class="item">
                    <img src="images/img_avatar3.png" alt="logo" class="img-fluid ">
                    <p class="font-weight-bold book-title" >Out
                        Of the Box</p>
                    <p class="text-muted " >by John Doe</p>
                    <div class="stars ">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>&nbsp;
                        <small class="font-weight-bold">(5)</small>
                    </div>
                </div>
                <div class="item ">
                    <img src="images/img_avatar3.png" alt="logo" class="img-fluid ">
                    <p class="font-weight-bold book-title" >Out
                        Of the Box</p>
                    <p class="text-muted " >by John Doe</p>
                    <div class="stars ">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>&nbsp;
                        <small class="font-weight-bold">(5)</small>
                    </div>
                </div>
                <div class="item ">
                    <img src="images/img_avatar3.png" alt="logo" class="img-fluid ">
                    <p class="font-weight-bold book-title" >Out
                        Of the Box</p>
                    <p class="text-muted ">by John Doe</p>
                    <div class="stars ">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>&nbsp;
                        <small class="font-weight-bold">(5)</small>
                    </div>
                </div>
                <div class="item ">
                    <img src="images/img_avatar3.png" alt="logo" class="img-fluid ">
                    <p class="font-weight-bold book-title" >Out
                        Of the Box</p>
                    <p class="text-muted ">by John Doe</p>
                    <div class="stars ">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>&nbsp;
                        <small class="font-weight-bold">(5)</small>
                    </div>
                </div>
                <div class="item ">
                    <img src="images/img_avatar3.png" alt="logo" class="img-fluid ">
                    <p class="font-weight-bold book-title" >Out
                        Of the Box</p>
                    <p class="text-muted">by John Doe</p>
                    <div class="stars ">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>&nbsp;
                        <small class="font-weight-bold">(5)</small>
                    </div>
                </div>
                <div class="item ">
                    <img src="images/img_avatar3.png" alt="logo" class="img-fluid ">
                    <p class="font-weight-bold book-title" >Out
                        Of the Box</p>
                    <p class="text-muted ">by John Doe</p>
                    <div class="stars ">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>&nbsp;
                        <small class="font-weight-bold">(5)</small>
                    </div>
                </div>
                <div class="item ">
                    <img src="images/img_avatar3.png" alt="logo" class="img-fluid ">
                    <p class="font-weight-bold book-title">Out
                        Of the Box</p>
                    <p class="text-muted ">by John Doe</p>
                    <div class="stars ">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>&nbsp;
                        <small class="font-weight-bold">(5)</small>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                     <p class="font-weight-bold align-center" >Powered By Bookchor</p>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                     <p class="align-center">&copy;Copyright-2018. All Rights Reserved</p>
                </div>
            </div>     
        </div>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="OwlCarousel2/dist/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>