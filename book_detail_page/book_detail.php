<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>BOOK DETAIL</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://assets.bookchor.xyz/global/vendor/bootstrap-4/css/bootstrap.min.css">
    <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Montserrat:400' >
    <link rel="stylesheet" type="text/css" href="http://assets.bookchor.xyz/global/css/global.css">
    <link rel="stylesheet" type="text/css" href="http://assets.bookchor.xyz/global/css/element.css">
    <link rel="stylesheet" href="../header/css/header.css">
    <link rel="stylesheet" href="../footer/css/footer.css">
    <link rel="stylesheet" href="../OwlCarousel2/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="../OwlCarousel2/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="../assets/bookchor_icons/styles.css">
    <link rel="stylesheet" href="css/book_detail.css">
</head>
<body>
    <?php include "../header/header.php";?>
    <section>
        <div class="container">
            <div class="card test">
                <div class="card-body" style="padding:10px">
                    <div class="grid">
                        <div class="card-body">
                            <img src="../images/book_image.svg" alt="logo" class="img-fluid mx-auto d-block">
                        </div>
                        <div class="card-body">
                            <small class="text-muted float-left">Book Name</small>
                            <small class="float-right right-content">
                                <div class="grid2">
                                    <p class="font-weight-bold float-left" >145</p>
                                    <p class="text-muted float-left" >Reviews</p>

                                    <p class="font-weight-bold float-left" >(4.5)</p>
                                    <i class="bc-bc-star star_icon align-center"></i>
                                </div>
                            </small>
                            <br>
                            <p class="font-weight-bold float-left" >Out of the
                                Box..</p>
                                <br>
                                <small class="text-muted float-left">Author Name</small>
                                <br>
                                <p class="font-weight-bold float-left" >BY JOHN DUE</p>
                                <br>
                                <small class="text-muted float-left">Publisher</small>
                                <br>
                                <p class="font-weight-bold float-left" >Jaico Publishing House</p>
                                <br>
                                <small class="text-muted float-left">Description</small>
                                <br>
                                <p class="float-left">Lorem ipsum dolor sit amet consectetur
                                    adipisicing elit. Inventore, consequatur.</p>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </div>
                        <div class="card-body return-date">
                            <div class="grid3">
                                <div class="card-body one">
                                    <button class="btn2 font-weight-bold"><a href="#">Issue Book</a></button>
                                </div>
                                <div class="card-body two">
                                    <button class="btn2 font-weight-bold"><a href="#">Find Book</a></button>
                                </div>
                                <div class="card-body three">
                                    <button class="btn2 font-weight-bold" type="button" data-toggle="modal" data-target="#exampleModalCenter"><a
                                        href="#">Recommend Book</a></button>
                                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
                                        aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title font-weight-bold" id="exampleModalLongTitle">Book</h5>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group has-search">
                                                        <input type="text" class="form-control" placeholder="Search for students..">
                                                    </div>
                                                    <div>
                                                        <button class="student_profile_logo">K</button>
                                                    </div>
                                                    <p class="float-left font-weight-bold">Mayank Haldunia</p>
                                                    <i class="bc-bc-tick-black float-right tick_sign"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card others">
                        <div class="card-body" style="padding:10px">
                            <p class="font-weight-bold float-left" >Others</p>
                            <br>
                            <div class="grid4">
                                <div class="others-content" >
                                    <div class="grid7">
                                        <p class="text-muted float-left" >ISBN</p>
                                        <p class="font-weight-bold float-right" >#4444444444444</p>
                                    </div>
                                </div>
                                <div class="others-content">
                                    <div class="grid7">
                                        <p class="text-muted float-left" >Edition</p>
                                        <p class="font-weight-bold float-right">First Edition</p>
                                    </div>
                                </div>
                                <div class="others-content">
                                    <div class="grid7">
                                        <p class="text-muted float-left" >Cover</p>
                                        <p class="font-weight-bold float-right" >Hard Cover</p>
                                    </div>
                                </div>
                                <div class="others-content">
                                    <div class="grid7">
                                        <p class="text-muted float-left" >MRP</p>
                                        <p class="font-weight-bold float-right">120/-</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="container">
                    <div class="card reviews">
                        <div class="card-body">
                            <p class="font-weight-bold float-left">Reviews</p>
                            <button class="btn1 float-right" type="button" data-toggle="modal" data-target="#myModal">Write
                                Review</button>
                                <div class="modal fade" id="myModal">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <!-- Modal Header -->
                                            <div class="modal-header" >
                                                <h5 class="modal-title">Select</h5>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <!-- Modal body -->
                                            <div class="modal-body">
                                                <i class="bc-bc-star review_star align-center"></i>&nbsp;
                                                <i class="bc-bc-star review_star align-center"></i>&nbsp;
                                                <i class="bc-bc-star review_star align-center"></i>&nbsp;
                                                <i class="bc-bc-star review_star align-center"></i>&nbsp;
                                                <i class="bc-bc-star review_star align-center"></i>
                                                <br><br>
                                                <input type="text" class="write-review" placeholder="Write Review">
                                            </div>
                                            <!-- Modal footer -->
                                            <div class="modal-footer">
                                                <button type="button" class="btn font-weight-bold" data-dismiss="modal">Cancel</button>
                                                <button type="button" class="btn font-weight-bold" style="background:#F2A614">Post</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body" style="padding:10px !important">
                                <div class="grid5">
                                    <div class="others-content" >
                                        <div class="grid6">
                                            <div>
                                                <button class="student_profile_logo float-left mt-0">K</button>
                                            </div>
                                            <div>
                                                <p class="font-weight-bold float-left">Mayank Haldunia (5)</p>
                                                &nbsp;
                                                <i class="bc-bc-star star_icon "></i>
                                                <br>
                                                <p class="text-muted float-left" >This is really nice book...<small><a href="#">Read More</a></small></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="others-content">
                                        <div class="grid6">
                                            <div>
                                                <button class="student_profile_logo float-left mt-0">K</button>
                                            </div>
                                            <div>
                                                <p class="font-weight-bold float-left"> Mayank Haldunia (5)</p>
                                                &nbsp;
                                                <i class="bc-bc-star star_icon "></i>
                                                <br>
                                                <p class="text-muted float-left" >This is really nice book...<small><a href="#">Read More</a></small></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="others-content">
                                        <div class="grid6">
                                            <div>
                                                <button class="student_profile_logo float-left mt-0">K</button>
                                            </div>
                                            <div>
                                                <p class="font-weight-bold float-left">Mayank Haldunia (5)</p>
                                                &nbsp;
                                                <i class="bc-bc-star star_icon "></i>
                                                <br>
                                                <p class="text-muted float-left" >This is really nice book...<small><a href="#">Read More</a></small></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="others-content">
                                        <div class="grid6">
                                            <div>
                                                <button class="student_profile_logo float-left mt-0">K</button>
                                            </div>
                                            <div>
                                                <p class="font-weight-bold float-left" >Mayank Haldunia (5)</p>
                                                &nbsp;
                                                <i class="bc-bc-star star_icon"></i>
                                                <br>
                                                <p class="text-muted float-left">This is really nice book...<small><a href="#">Read More</a></small></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="slider-content" >
                    <div class="container">
                        <div class="card books-you-might-like">
                            <div class="card-body books">
                                <p class="font-weight-bold float-left" >Books You Might Like</p>
                            </div>
                        </div>
                        <div id="owl-test" class="owl-carousel owl-theme">
                            <div class="item ">
                                <img src="../images/img_avatar3.png" alt="logo" class="img-fluid ">
                                <p class="font-weight-bold book-title" >Out
                                    Of the Box</p>
                                    <p class="text-muted " >by John Doe</p>
                                    <div class="stars ">
                                        <i class="bc-bc-star star_icon"></i>
                                        <i class="bc-bc-star star_icon"></i>
                                        <i class="bc-bc-star star_icon"></i>
                                        <i class="bc-bc-star star_icon"></i>
                                        <i class="bc-bc-star star_icon"></i>&nbsp;
                                        <small class="font-weight-bold">(5)</small>
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="../images/img_avatar3.png" alt="logo" class="img-fluid ">
                                    <p class="font-weight-bold book-title" >Out
                                        Of the Box</p>
                                        <p class="text-muted " >by John Doe</p>
                                        <div class="stars ">
                                            <i class="bc-bc-star star_icon"></i>
                                            <i class="bc-bc-star star_icon"></i>
                                            <i class="bc-bc-star star_icon"></i>
                                            <i class="bc-bc-star star_icon"></i>
                                            <i class="bc-bc-star star_icon"></i>&nbsp;
                                            <small class="font-weight-bold">(5)</small>
                                        </div>
                                    </div>
                                    <div class="item ">
                                        <img src="../images/img_avatar3.png" alt="logo" class="img-fluid ">
                                        <p class="font-weight-bold book-title" >Out
                                            Of the Box</p>
                                            <p class="text-muted " >by John Doe</p>
                                            <div class="stars ">
                                                <i class="bc-bc-star star_icon"></i>
                                                <i class="bc-bc-star star_icon"></i>
                                                <i class="bc-bc-star star_icon"></i>
                                                <i class="bc-bc-star star_icon"></i>
                                                <i class="bc-bc-star star_icon"></i>&nbsp;
                                                <small class="font-weight-bold">(5)</small>
                                            </div>
                                        </div>
                                        <div class="item ">
                                            <img src="../images/img_avatar3.png" alt="logo" class="img-fluid ">
                                            <p class="font-weight-bold book-title" >Out
                                                Of the Box</p>
                                                <p class="text-muted ">by John Doe</p>
                                                <div class="stars ">
                                                    <i class="bc-bc-star star_icon"></i>
                                                    <i class="bc-bc-star star_icon"></i>
                                                    <i class="bc-bc-star star_icon"></i>
                                                    <i class="bc-bc-star star_icon"></i>
                                                    <i class="bc-bc-star star_icon"></i>&nbsp;
                                                    <small class="font-weight-bold">(5)</small>
                                                </div>
                                            </div>
                                            <div class="item ">
                                                <img src="../images/img_avatar3.png" alt="logo" class="img-fluid ">
                                                <p class="font-weight-bold book-title" >Out
                                                    Of the Box</p>
                                                    <p class="text-muted ">by John Doe</p>
                                                    <div class="stars ">
                                                        <i class="bc-bc-star star_icon"></i>
                                                        <i class="bc-bc-star star_icon"></i>
                                                        <i class="bc-bc-star star_icon"></i>
                                                        <i class="bc-bc-star star_icon"></i>
                                                        <i class="bc-bc-star star_icon"></i>&nbsp;
                                                        <small class="font-weight-bold">(5)</small>
                                                    </div>
                                                </div>
                                                <div class="item ">
                                                    <img src="../images/img_avatar3.png" alt="logo" class="img-fluid ">
                                                    <p class="font-weight-bold book-title" >Out
                                                        Of the Box</p>
                                                        <p class="text-muted">by John Doe</p>
                                                        <div class="stars ">
                                                            <i class="bc-bc-star star_icon"></i>
                                                            <i class="bc-bc-star star_icon"></i>
                                                            <i class="bc-bc-star star_icon"></i>
                                                            <i class="bc-bc-star star_icon"></i>
                                                            <i class="bc-bc-star star_icon"></i>&nbsp;
                                                            <small class="font-weight-bold">(5)</small>
                                                        </div>
                                                    </div>
                                                    <div class="item ">
                                                        <img src="../images/img_avatar3.png" alt="logo" class="img-fluid ">
                                                        <p class="font-weight-bold book-title" >Out
                                                            Of the Box</p>
                                                            <p class="text-muted ">by John Doe</p>
                                                            <div class="stars ">
                                                                <i class="bc-bc-star star_icon"></i>
                                                                <i class="bc-bc-star star_icon"></i>
                                                                <i class="bc-bc-star star_icon"></i>
                                                                <i class="bc-bc-star star_icon"></i>
                                                                <i class="bc-bc-star star_icon"></i>&nbsp;
                                                                <small class="font-weight-bold">(5)</small>
                                                            </div>
                                                        </div>
                                                        <div class="item ">
                                                            <img src="../images/img_avatar3.png" alt="logo" class="img-fluid ">
                                                            <p class="font-weight-bold book-title">Out
                                                                Of the Box</p>
                                                                <p class="text-muted ">by John Doe</p>
                                                                <div class="stars ">
                                                                    <i class="bc-bc-star star_icon"></i>
                                                                    <i class="bc-bc-star star_icon"></i>
                                                                    <i class="bc-bc-star star_icon"></i>
                                                                    <i class="bc-bc-star star_icon"></i>
                                                                    <i class="bc-bc-star star_icon"></i>&nbsp;
                                                                    <small class="font-weight-bold">(5)</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <?php include "../footer/footer.php";?>
                                            </body>
                                            <script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/jquery.js"></script>
                                            <script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/popper.js"></script>
                                            <script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/bootstrap.min.js"></script>
                                            <script src="../OwlCarousel2/dist/owl.carousel.min.js"></script>
                                            <script src="../js/main.js"></script>
                                            <script src="../header/js/header.js"></script>
                                            </html>
