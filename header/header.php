    <div class="container-fluid header_bar">
        <div class="row ">
            <div class="col-2 col-md-5 col-lg-4 pl-0 pr-0">
                <a href="/"><h6 id="brand-name" class="float-left font-weight-bold school_title">
                    <img src="http://library.bookchor.com/admin/assets/img/logos/oasis.png" height="50px;">
                    <span class="d-none d-md-inline-block">Oasis School Dehradun</span>
                </h6></a>
            </div>
            <div class="col-8 col-md-6 pt-2 pl-0 pr-0">
                <form>
                    <div class="search-condition-style d-none d-md-block">
						<i class="bc-bc-down-arrow dropdown_icon"></i>
						<select name="allbook" class="select-box-condition font-weight-bold" style="border-radius: 16px 0 0 16px !important;">
							<option value="allbook">All Books</option>
							<option value="ebook">E-Books</option>
						</select>
					</div>
                    <input type="text" class="search d-none d-md-block header_searchbar float-right" name="search" placeholder="Search Book">
				    <button class="header_searchicon d-none d-md-block"><i class="bc-bc-search-icon"></i><span class="pl-1 font-weight-bold">SEARCH</span></button>
                    <input type="text" class="search header_searchbar_mobile d-block d-md-none float-left" name="search" placeholder="Search Book">
                    <button class="header_searchicon_mobile d-block d-md-none"><i class="bc-bc-search-icon"></i></button>
                </form>
            </div>
            <div class="col-2 col-md-1 col-lg-2">
                <ul class="nav float-right">
                    <li class="nav-item">
                        <button class="profile mt-2" onclick="logout();">K</button>
                        <div class="log-out log-display" id="log-out">
                            <a class="dropdown-item" href="#">Settings</a>
                            <a class="dropdown-item" href="#">Wishlist</a>
                            <a class="dropdown-item" href="#">Issue History</a>
                            <a class="dropdown-item" href="#">Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
