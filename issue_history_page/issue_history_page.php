<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ISSUE HISTORY</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://assets.bookchor.xyz/global/vendor/bootstrap-4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat" >
    <link rel="stylesheet" href="../header/css/header.css">
    <link rel="stylesheet" href="../footer/css/footer.css">
    <link rel="stylesheet" href="../assets/bookchor_icons/styles.css">
    <link rel="stylesheet" href="css/issue_history_page.css">
</head>
<body>
    <?php include "../header/header.php";?>
    <section class="main">
        <div class="container-fluid outer">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <h6 class="font-weight-bold pl-4">Issue History</h6>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="row">
                        <div class="col-md-6 col-lg-4 col-xl-4">
                            <div class="card-body inner-content" >
                                <div class="grid2">
                                    <div class="card-body">
                                        <img src="../images/book_image.svg" alt="logo" class="img-fluid" style="width:100%;">
                                    </div>
                                    <div class="card-body">
                                        <h6 class="font-weight-bold float-left book-title" >Out of the
                                            Box</h6>
                                            <br>
                                            <small class="text-muted float-left">BY JOHN DOE</small>
                                            <br>
                                            <small>(12 reviews)</small>
                                            <button type="button" class="btn btn-success rating_button pl-0 pt-0 pb-0">5.0<span class="bc-bc-star star_icon"></button>                                            <br>
                                            <small style="text-align:justify !important;">
                                                Lorem ipsum dolor sit amet
                                                consectetur
                                                adipisicing elit. Culpa quos eos quis
                                                est nobis reiciendis officia voluptatum, et eligendi ipsa....</small>
                                                <br>
                                                <small class="font-weight-bold float-right" style="color:#108690"> Issue
                                                    Date</small>
                                                    <br>
                                                    <p class="font-weight-bold float-right" >
                                                        25 July-2018</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body return-date">
                                                <p class="align-center font-weight-bold">
                                                    Return Date: 5 August-2018</p>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="col-md-6 col-lg-4 col-xl-4 card ">
                                                <div class="card-body inner-content">
                                                    <div class="grid2">
                                                        <div class="card-body">
                                                            <img src="../images/book_image.svg" alt="logo" class="img-fluid" style="width:100%;">
                                                        </div>
                                                        <div class="card-body">
                                                            <h6 class="font-weight-bold float-left book-title" >Out of the
                                                                Box</h6>
                                                                <br>
                                                                <small class="text-muted float-left">BY JOHN DOE</small>
                                                                <br>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i> &nbsp;
                                                                <small>(5)</small>
                                                                <br>
                                                                <small>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa quos eos
                                                                    quis
                                                                    est nobis reiciendis officia voluptatum, et eligendi ipsa....</small>
                                                                    <br>
                                                                    <small class="font-weight-bold float-right" style="color:#108690"> Issue
                                                                        Date</small>
                                                                        <br>
                                                                        <p class="font-weight-bold float-right" >25-July-2018</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="card-body return-date">
                                                                <p class="align-center font-weight-bold">Return Date: 5-August-2018</p>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="col-md-6 col-lg-4 col-xl-4 card">
                                                            <div class="card-body inner-content">
                                                                <div class="grid2">
                                                                    <div class="card-body">
                                                                        <img src="../images/book_image.svg" alt="logo" class="img-fluid" style="width:100%;">
                                                                    </div>
                                                                    <div class="card-body">
                                                                        <h6 class="font-weight-bold float-left book-title" >Out of the
                                                                            Box</h6>
                                                                            <br>
                                                                            <small class="text-muted float-left">BY JOHN DOE</small>
                                                                            <br>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i> &nbsp;
                                                                            <small>(5)</small>
                                                                            <br>
                                                                            <small>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa quos eos
                                                                                quis
                                                                                est nobis reiciendis officia voluptatum, et eligendi ipsa....</small>
                                                                                <br>
                                                                                <small class="font-weight-bold float-right" style="color:#108690"> Issue
                                                                                    Date</small>
                                                                                    <br>
                                                                                    <p class="font-weight-bold float-right" >25-July-2018</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="card-body return-date">
                                                                            <p class="align-center font-weight-bold">Return Date: 5-August-2018</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-4 col-xl-4 card">
                                                                        <div class="card-body inner-content">
                                                                            <div class="grid2">
                                                                                <div class="card-body">
                                                                                    <img src="../images/book_image.svg" alt="logo" class="img-fluid" style="width:100%;">
                                                                                </div>
                                                                                <div class="card-body">
                                                                                    <h6 class="font-weight-bold float-left book-title" >Out of the
                                                                                        Box</h6>
                                                                                        <br>
                                                                                        <small class="text-muted float-left">BY JOHN DOE</small>
                                                                                        <br>
                                                                                        <i class="fas fa-star"></i>
                                                                                        <i class="fas fa-star"></i>
                                                                                        <i class="fas fa-star"></i>
                                                                                        <i class="fas fa-star"></i>
                                                                                        <i class="fas fa-star"></i> &nbsp;
                                                                                        <small>(5)</small>
                                                                                        <br>
                                                                                        <small>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa quos eos
                                                                                            quis
                                                                                            est nobis reiciendis officia voluptatum, et eligendi ipsa....</small>
                                                                                            <br>
                                                                                            <small class="font-weight-bold float-right" style="color:#108690"> Issue
                                                                                                Date</small>
                                                                                                <br>
                                                                                                <p class="font-weight-bold float-right" >25-July-2018</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="card-body return-date">
                                                                                        <p class="align-center font-weight-bold">Return Date: 5-August-2018</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </section>
                                                            <?php include "../footer/footer.php";?>
                                                        </body>
                                                        <script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/jquery.js"></script>
                                                        <script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/popper.js"></script>
                                                        <script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/bootstrap.min.js"></script>
                                                        <script src="../header/js/header.js"></script>
                                                        </html>
