$(document).ready(function() {
    $(".find").click(function() {
        $(this)
        .closest("div.test2")
        .find("div.book-availability")
        .toggleClass("test3");
    });
    $(".issue").click(function() {
        alert("clicked");
        $(this)
        .closest("div.test2").innerHTML("usr")
    });
    $(".add-to-wishlist").click(function() {
        $(this).toggleClass("test");
    });
    $(".navigate_button").click(function(){
        $(this).css("color","#108690");
    });
    $(".owl-carousel").owlCarousel({
        loop: true,
        addClassActive: true,
        margin: 10,
        rewind: true,
        responsiveClass: true,
        autoplay: true,
        nav: true,
        navText: ["<i class='bc-bc-left-arrow-1 black_color_type'></i>",
        "<i class='bc-bc-right-arrow black_color_type'></i>"],
        smartSpeed :900,
        responsive: {
            0: {
                items: 2,
                nav: true
            },
            600: {
                items: 3,
                nav: false
            },
            1000: {
                items: 4,
                nav: true,
                loop: false
            }
        },
    });
    $(".play").click(function() {
        owl.trigger("owl.play", 2000); //owl.play event accept autoPlay speed as second parameter
    });
});
