<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Landing Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="http://assets.bookchor.xyz/global/vendor/bootstrap-4/css/bootstrap.min.css">
    <link rel="stylesheet" href="OwlCarousel2/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="OwlCarousel2/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="css/landing_page.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="http://assets.bookchor.xyz/global/css/global.css">
    <link rel="stylesheet" type="text/css" href="http://assets.bookchor.xyz/global/css/element.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
        crossorigin="anonymous">
</head>

<body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark ">

        <div class="container-fluid">
            <h6 id="brand-name" class="float-left font-weight-bold">St-Stephen School</h6>

            <ul class="nav float-right" id="nav-list">
                <li class="nav-item">
                    <a class="nav-link " href="#"><i class="fas fa-heart"></i></a>
                </li>
                <li class="nav-item" id="user-history">
                    <a class="nav-link" href="#">Issue History</a>
                </li>
                <li class="nav-item" id="user-clock">
                    <a class="nav-link" href="#"><img src="images/History_icon.svg" class="img-fluid" alt="icon"></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fas fa-user-circle"></i></a>
                </li>
            </ul>
        </div>

    </nav>

    <nav class="navbar navbar-expand-sm bg-dark navbar-dark second-nav">
        <div class="container-fluid">
            <div class="row">

                <div class="col-6 col-sm-6 col-md-10 col-lg-10 col-xl-10" style="padding-right:0">
                        <ul class="nav float-left " id="nav-items">
                        <li class="nav-item active" style="margin:5px ">
                            <button class="btn"><a href="#">All Books</a></button>
                        </li>
                        <li class="nav-item" style="margin:5px ">
                            <button class="btn"><a href="#">Crime</a></button>
                        </li>
                        <li class="nav-item" style="margin:5px ">
                            <button class="btn"><a href="#">Romance</a></button>
                        </li>
                        <li class="nav-item" style="margin:5px ">
                            <button class="btn"><a href="#">Literature</a></button>
                        </li>
                        <li class="nav-item" style="margin:5px ">
                            <button class="btn"><a href="#">Sci-Fi</a></button>
                        </li>
                        <li class="nav-item" style="margin:5px ">
                            
                            <div class="dropdown">
                                <button class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    More
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Literature</a>
                                    <a class="dropdown-item" href="#">Literature</a>
                                    <a class="dropdown-item" href="#">Literature</a>
                                </div>
                            </div>
                        </li>
                    </ul>

                    <div class="dropdown categories">
                        <button class=" dropdown-toggle category-button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            Categories
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">All Books</a>
                            <a class="dropdown-item" href="#">Romance</a>
                            <a class="dropdown-item" href="#">Crime</a>
                            <a class="dropdown-item" href="#">Literature</a>
                            <a class="dropdown-item" href="#">Sci-Fi</a>
                            <a class="dropdown-item" href="#">More</a>
                        </div>
                     </div>
                </div>

                <div class="col-6 col-sm-6 col-md-2 col-lg-2 col-xl-2" style="padding-right:0">
                    <form class="form-inline">
                         <input class="form-control float-right" type="search" placeholder="Search Books" aria-label="Search">
                    </form>
                </div>
            </div>
                
        </div>
    </nav>

    <section>
        <div id="demo" class="carousel " data-ride="carousel">

            
                <div class="carousel-item active">
                    <img src="images/Background Image.svg" class="img-fluid w3-animate-fading" alt="Artful Storytelling">
                </div>
                <div class="carousel-item">
                    <img src="images/slider3.jpg" class="img-fluid w3-animate-fading" alt="Artful Storytelling">
                </div>
                <div class="carousel-item">
                    <img src="images/Background Image.svg" class="img-fluid w3-animate-fading" alt="Artful Storytelling">
                </div>
          

            <!-- Left and right controls -->
            <!-- <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a> -->
        </div>
    </section>


    <section>
        <div class="container-fluid">
            <div class="card">
                <div class="card-body books">
                    <p class="font-weight-bold float-left title1">
                        Trending Books</p>
                </div>
            </div>

            <div id="owl-demo" class="owl-carousel owl-theme">

                <div class="item">
                    <div>
                        <img src="images/Book Image.svg" alt="logo" class="img-fluid mx-auto round">
                        <div class="card-body slide-content">
                            <p class="font-weight-bold book-title">Out
                                Of the Box</p>
                            <p class="text-muted " >by John Doe</p>
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>&nbsp;
                                <small class="font-weight-bold">(5)</small>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="item">
                    <div>
                        <img src="images/Book Image.svg" alt="logo" class="img-fluid mx-auto round">
                        <div class="card-body slide-content">
                            <p class="font-weight-bold book-title">Out
                                Of the Box</p>
                            <p class="text-muted " >by John Doe</p>
                            <div class="stars ">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>&nbsp;
                                <small class="font-weight-bold">(5)</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div>
                        <img src="images/Book Image.svg" alt="logo" class="img-fluid mx-auto round">
                        <div class="card-body slide-content">
                            <p class="font-weight-bold book-title" >Out
                                Of the Box</p>
                            <p class="text-muted " >by John Doe</p>
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>&nbsp;
                                <small class="font-weight-bold">(5)</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div>
                        <img src="images/Book Image.svg" alt="logo" class="img-fluid mx-auto round">
                        <div class="card-body slide-content">
                            <p class="font-weight-bold book-title" >Out
                                Of the Box</p>
                            <p class="text-muted " >by John Doe</p>
                            <div class="stars ">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>&nbsp;
                                <small class="font-weight-bold">(5)</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div>
                        <img src="images/Book Image.svg" alt="logo" class="img-fluid mx-auto round">
                        <div class="card-body slide-content">
                            <p class="font-weight-bold book-title" >Out
                                Of the Box</p>
                            <p class="text-muted " >by John Doe</p>
                            <div class="stars ">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>&nbsp;
                                <small class="font-weight-bold">(5)</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div>
                        <img src="images/Book Image.svg" alt="logo" class="img-fluid mx-auto round">
                        <div class="card-body book-content slide-content">
                            <p class="font-weight-bold book-title" >Out
                                Of the Box</p>
                            <p class="text-muted " >by John Doe</p>
                            <div class="stars ">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>&nbsp;
                                <small class="font-weight-bold">(5)</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div>
                        <img src="images/Book Image.svg" alt="logo" class="img-fluid mx-auto round">
                        <div class="card-body slide-content">
                            <p class="font-weight-bold book-title" >Out
                                Of the Box</p>
                            <p class="text-muted" >by John Doe</p>
                            <div class="stars ">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>&nbsp;
                                <small class="font-weight-bold">(5)</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section>
        <div class="container-fluid">
            <div class="card">
                <div class="card-body books">
                    <p class="font-weight-bold float-left title2">
                        Wishlist Books</p>
                    <button class="btn1 float-right"><a href="wishlist_page.php">More</a></button> 
                </div>
            </div>

            <div id="owl-demo" class="owl-carousel owl-theme">

                <div class="item">
                    <div>
                        <img src="images/Book Image.svg" alt="logo" class="img-fluid mx-auto round">
                        <div class="card-body slide-content">
                            <p class="font-weight-bold book-title">Out
                                Of the Box</p>
                            <p class="text-muted" >by John Doe</p>
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>&nbsp;
                                <small class="font-weight-bold">(5)</small>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="item">
                    <div>
                        <img src="images/Book Image.svg" alt="logo" class="img-fluid mx-auto round">
                        <div class="card-body slide-content">
                            <p class="font-weight-bold book-title">Out
                                Of the Box</p>
                            <p class="text-muted " >by John Doe</p>
                            <div class="stars ">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>&nbsp;
                                <small class="font-weight-bold">(5)</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div>
                        <img src="images/Book Image.svg" alt="logo" class="img-fluid mx-auto round">
                        <div class="card-body slide-content">
                            <p class="font-weight-bold book-title" >Out
                                Of the Box</p>
                            <p class="text-muted " >by John Doe</p>
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>&nbsp;
                                <small class="font-weight-bold">(5)</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div>
                        <img src="images/Book Image.svg" alt="logo" class="img-fluid mx-auto round">
                        <div class="card-body slide-content">
                            <p class="font-weight-bold book-title" >Out
                                Of the Box</p>
                            <p class="text-muted " >by John Doe</p>
                            <div class="stars ">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>&nbsp;
                                <small class="font-weight-bold">(5)</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div>
                        <img src="images/Book Image.svg" alt="logo" class="img-fluid mx-auto round">
                        <div class="card-body slide-content">
                            <p class="font-weight-bold book-title" >Out
                                Of the Box</p>
                            <p class="text-muted " >by John Doe</p>
                            <div class="stars ">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>&nbsp;
                                <small class="font-weight-bold">(5)</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div>
                        <img src="images/Book Image.svg" alt="logo" class="img-fluid mx-auto round">
                        <div class="card-body book-content slide-content">
                            <p class="font-weight-bold book-title" >Out
                                Of the Box</p>
                            <p class="text-muted " >by John Doe</p>
                            <div class="stars ">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>&nbsp;
                                <small class="font-weight-bold">(5)</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div>
                        <img src="images/Book Image.svg" alt="logo" class="img-fluid mx-auto round">
                        <div class="card-body slide-content">
                            <p class="font-weight-bold book-title" >Out
                                Of the Box</p>
                            <p class="text-muted" >by John Doe</p>
                            <div class="stars ">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>&nbsp;
                                <small class="font-weight-bold">(5)</small>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- <div id="demo" class="carousel slide" data-ride="carousel">
                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#demo" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                    </a>
                </div> <button class="btn1 float-right"><a href="wishlist_page.php">More</a></button> -->
            </div>

        </div>
    </section>

    <section class="author-content">
        <p class="font-weight-bold align-center title4">AUTHOR OF THE MONTH</p>
        <div class="container">
            <div class="card author-of-month round">
                <div class="card-body" style="padding:10px !important">
                    <div class="grid2">
                        <div class="card-body">
                            <img src="images/avatar4.jpg" alt="logo" class="img-fluid mx-auto d-block round">

                        </div>
                        <div class="card-body">

                            <small class="text-muted float-left">Book Name :</small>
                            <small class="font-weight-bold float-left small-text">&nbsp;Out of the Box</small>

                            <br>

                            <small class="text-muted float-left">Author Name :</small>

                            <small class="font-weight-bold float-left small-text" >&nbsp;JOHN DOE</small>
                            <br>

                            <small class=" float-left">Description</small>
                            <br>
                            <small class="float-left font-weight-bold" style="text-align:left">Lorem ipsum
                                dolor sit amet consectetur
                                adipisicing elit</small>
                            <br><br>
                            <a href="#" class="float-right view-more-link" style=" ">View More</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="reader-content">
        <div class="container-fluid">

            <div class="card">
                <div class="card-body books">
                    <p class="font-weight-bold float-left title3">
                        Club Reader</p>
                    <button class="btn1 float-right"><a href="#">More</a></button>
                </div>
            </div>

            <div id="owl-test" class="owl-carousel owl-theme">
                <div class="item club-items">
                    <img src="images/img_avatar3.png" alt="logo" class="img-fluid club-image">
                    <button class="btn3 literature">Literature</button>
                    <p class="font-weight-bold grp-name" >
                        Group: A
                    </p>
                </div>

                <div class="item club-items">
                    <img src="images/img_avatar3.png" alt="logo" class="img-fluid club-image">
                    <button class="btn3 sci-fi" >Literature</button>
                    <p class="font-weight-bold grp-name" >Group: A</p>
                </div>
                <div class="item club-items">
                    <img src="images/img_avatar3.png" alt="logo" class="img-fluid club-image">
                    <button class="btn3 romance" >Literature</button>
                    <p class="font-weight-bold grp-name">Group: A</p>
                </div>
                <div class="item club-items">
                    <img src="images/img_avatar3.png" alt="logo" class="img-fluid club-image">
                    <button class="btn3 crime" >Literature</button>
                    <p class="font-weight-bold grp-name">Group: A</p>
                </div>
                <div class="item club-items">
                    <img src="images/img_avatar3.png" alt="logo" class="img-fluid club-image">
                    <button class="btn3 sci-fi">Literature</button>
                    <p class="font-weight-bold grp-name">Group: A</p>
                </div>
                <div class="item club-items">
                    <img src="images/img_avatar3.png" alt="logo" class="img-fluid club-image">
                    <button class="btn3 crime" >Literature</button>
                    <p class="font-weight-bold grp-name">Group: A</p>
                </div>
                
            </div>

        </div>


    </section>

    <section class="last-content">
        <h6 class="font-weight-bold align-center title5">STUDENT OF THE MONTH</h6>

        <div class="container">

            <div class="card student-of-month round">
                <div class="card-body" style="padding:10px !important">
                    <div class="grid2">
                        <div class="card-body">
                            <img src="images/avatar4.jpg" alt="logo" class="img-fluid mx-auto d-block round">

                        </div>
                        <div class="card-body">

                            <small class="font-weight-bold float-left">Student Name :</small>
                            <small class=" float-left font-weight-bold" >&nbsp;John Doe</small>

                            <br>

                            <small class="font-weight-bold float-left">Read Books :</small>

                            <small class=" float-left font-weight-bold">&nbsp;50 Books</small>
                            <br>

                            <small class="font-weight-bold float-left">Favourite Categories</small>
                            <br>

                            <div class="grid3">
                                <button class="btn2 literature">Literature</button>
                                <button class="btn2 romance">Romance</button>
                                <button class="btn2 youth-age">Youth Age</button>
                                <button class="btn2 crime">Crime</button>
                                <button class="btn2 sci-fi">Sci-Fi</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                     <p class="font-weight-bold align-center" >Powered By Bookchor</p>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                     <p class="align-center">&copy;Copyright-2018. All Rights Reserved</p>
                </div>
            </div>     
        </div>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="OwlCarousel2/dist/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>