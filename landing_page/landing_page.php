<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Landing Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat" >
    <link rel="stylesheet" href="http://assets.bookchor.xyz/global/vendor/bootstrap-4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../header/css/header.css">
    <link rel="stylesheet" href="../footer/css/footer.css">
    <link rel="stylesheet" href="../OwlCarousel2/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="../OwlCarousel2/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="../assets/bookchor_icons/styles.css">
    <link rel="stylesheet" href="css/landing_page.css">
</head>
<body>
    <?php include "../header/header.php";?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-6 col-md-10 col-lg-8">
                <div class="d-none d-md-flex mt-2 mb-2">
                    <button type="button" class="btn btn-outline-primary mr-2  rounded ">All Books</button>
                    <button type="button" class="btn btn-outline-secondary mr-2  rounded ">Crime</button>
                    <button type="button" class="btn btn-outline-success mr-2 rounded ">Romance</button>
                    <button type="button" class="btn btn-outline-info mr-2  rounded ">Literature</button>
                    <button type="button" class="btn btn-outline-warning mr-2  rounded ">Sci-Fi</button>
                    <button type="button" class="btn btn-outline-danger mr-2  rounded ">Danger</button>
                    <div class="dropdown">
                        <button type="button" class="btn mr-2 btn-outline-dark" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</button>
                        <div class="dropdown-menu p-0" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Literature</a>
                            <a class="dropdown-item" href="#">Literature</a>
                            <a class="dropdown-item" href="#">Literature</a>
                        </div>
                    </div>
                </div>
                <div class="dropdown categories d-block d-md-none pt-2">
                    <button class=" dropdown-toggle category-button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Categories
                </button>
                <div class="dropdown-menu p-0" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">All Books</a>
                    <a class="dropdown-item" href="#">Romance</a>
                    <a class="dropdown-item" href="#">Crime</a>
                    <a class="dropdown-item" href="#">Literature</a>
                    <a class="dropdown-item" href="#">Sci-Fi</a>
                    <a class="dropdown-item" href="#">E-books</a>
                    <a class="dropdown-item" href="#">More</a>
                </div>
            </div>
        </div>
        <div class="col-6 col-md-2 col-lg-4 pl-0 mt-2 mb-2">
                <button type="button" class="btn btn-outline-light text-dark float-right">E-books</button>
            <!-- <form>
                <input type="text" class="search_inline" placeholder="Search Book"/><span class="bc-bc-search-icon search_icon pt-md-3 pt-2"></span>
            </form> -->
        </div>
    </div>
    <section>
        <div id="demo" class="carousel banner_image" data-ride="carousel">
            <div class="carousel-item active">
                <img src="../images/Background_Image.svg" class="img-fluid w3-animate-fading banner_image " alt="Artful Storytelling">
            </div>
            <div class="carousel-item">
                <img src="../images/slider3.jpg" class="img-fluid w3-animate-fading banner_image" alt="Artful Storytelling">
            </div>
            <div class="carousel-item">
                <img src="../images/Background_Image.svg" class="img-fluid w3-animate-fading banner_image" alt="Artful Storytelling">
            </div>
        </div>
    </section>
<section>
<!-- <div class="container-fluid"> -->
<div class="card">
<div class="card-body books">
    <p class="font-weight-bold float-left title1">
        Trending Books</p>
    </div>
</div>
<div class="owl-carousel owl-theme">
    <div class="item">
        <div>
            <div class="img-box ">
                <img src="../images/book_image.svg" alt="logo" class="img-fluid round club-image w-100">
        </div>
            <div class="card-body slide-content p-0">
                <p class="font-weight-bold book-title">Out
                    Of the Box</p>
                    <p class="text-muted " >by John Doe</p>
                    <div class="stars">
                        <i class="bc-bc-star star_icon"></i>
                        <i class="bc-bc-star star_icon"></i>
                        <i class="bc-bc-star star_icon"></i>
                        <i class="bc-bc-star star_icon"></i>
                        <i class="bc-bc-star star_icon"></i>&nbsp;
                        <small class="font-weight-bold">(5)</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div>
                <img src="../images/book_image.svg" alt="logo" class="img-fluid round club-image w-100">
                <div class="card-body slide-content p-0">
                    <p class="font-weight-bold book-title">Out
                        Of the Box</p>
                        <p class="text-muted " >by John Doe</p>
                        <div class="stars ">
                            <i class="bc-bc-star star_icon"></i>
                            <i class="bc-bc-star star_icon"></i>
                            <i class="bc-bc-star star_icon"></i>
                            <i class="bc-bc-star star_icon"></i>
                            <i class="bc-bc-star star_icon"></i>&nbsp;
                            <small class="font-weight-bold">(5)</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div>
                    <img src="../images/book_image.svg" alt="logo" class="img-fluid round club-image w-100">
                    <div class="card-body slide-content p-0">
                        <p class="font-weight-bold book-title" >Out
                            Of the Box</p>
                            <p class="text-muted " >by John Doe</p>
                            <div class="stars">
                                <i class="bc-bc-star star_icon"></i>
                                <i class="bc-bc-star star_icon"></i>
                                <i class="bc-bc-star star_icon"></i>
                                <i class="bc-bc-star star_icon"></i>
                                <i class="bc-bc-star star_icon"></i>&nbsp;
                                <small class="font-weight-bold">(5)</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div>
                        <img src="../images/book_image.svg" alt="logo" class="img-fluid round club-image w-100">
                        <div class="card-body slide-content p-0">
                            <p class="font-weight-bold book-title" >Out
                                Of the Box</p>
                                <p class="text-muted " >by John Doe</p>
                                <div class="stars ">
                                    <i class="bc-bc-star star_icon"></i>
                                    <i class="bc-bc-star star_icon"></i>
                                    <i class="bc-bc-star star_icon"></i>
                                    <i class="bc-bc-star star_icon"></i>
                                    <i class="bc-bc-star star_icon"></i>&nbsp;
                                    <small class="font-weight-bold">(5)</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div>
                            <img src="../images/book_image.svg" alt="logo" class="img-fluid round club-image w-100">
                            <div class="card-body slide-content p-0">
                                <p class="font-weight-bold book-title" >Out
                                    Of the Box</p>
                                    <p class="text-muted " >by John Doe</p>
                                    <div class="stars ">
                                        <i class="bc-bc-star star_icon"></i>
                                        <i class="bc-bc-star star_icon"></i>
                                        <i class="bc-bc-star star_icon"></i>
                                        <i class="bc-bc-star star_icon"></i>
                                        <i class="bc-bc-star star_icon"></i>&nbsp;
                                        <small class="font-weight-bold">(5)</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div>
                                <img src="../images/book_image.svg" alt="logo" class="img-fluid round club-image w-100">
                                <div class="card-body book-content slide-content p-0">
                                    <p class="font-weight-bold book-title" >Out
                                        Of the Box</p>
                                        <p class="text-muted " >by John Doe</p>
                                        <div class="stars ">
                                            <i class="bc-bc-star star_icon"></i>
                                            <i class="bc-bc-star star_icon"></i>
                                            <i class="bc-bc-star star_icon"></i>
                                            <i class="bc-bc-star star_icon"></i>
                                            <i class="bc-bc-star star_icon"></i>&nbsp;
                                            <small class="font-weight-bold">(5)</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <img src="../images/book_image.svg" alt="logo" class="img-fluid round club-image w-100">
                                    <div class="card-body slide-content p-0">
                                        <p class="font-weight-bold book-title" >Out
                                            Of the Box</p>
                                            <p class="text-muted" >by John Doe</p>
                                            <div class="stars ">
                                                <i class="bc-bc-star star_icon"></i>
                                                <i class="bc-bc-star star_icon"></i>
                                                <i class="bc-bc-star star_icon"></i>
                                                <i class="bc-bc-star star_icon"></i>
                                                <i class="bc-bc-star star_icon"></i>&nbsp;
                                                <small class="font-weight-bold">(5)</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <!-- </div> -->
                    </section>
<section>
<!-- <div class="container-fluid"> -->
<div class="card">
<div class="card-body books">
<p class="font-weight-bold float-left title2">
Wishlist Books</p>
<button class="btn1 float-right"><a href="../wishlist_page/wishlist_page.php">More</a></button>
</div>
</div>

<div class="owl-carousel owl-theme">

<div class="item">
<div>
<img src="../images/book_image.svg" alt="logo" class="img-fluid round club-image w-100">
<div class="card-body slide-content p-0">
<p class="font-weight-bold book-title">Out
    Of the Box</p>
    <p class="text-muted" >by John Doe</p>
    <div class="stars">
        <i class="bc-bc-star star_icon"></i>
        <i class="bc-bc-star star_icon"></i>
        <i class="bc-bc-star star_icon"></i>
        <i class="bc-bc-star star_icon"></i>
        <i class="bc-bc-star star_icon"></i>&nbsp;
        <small class="font-weight-bold">(5)</small>
    </div>
</div>

</div>
</div>
<div class="item">
<div>
<img src="../images/book_image.svg" alt="logo" class="img-fluid round club-image w-100">
<div class="card-body slide-content p-0">
    <p class="font-weight-bold book-title">Out
        Of the Box</p>
        <p class="text-muted " >by John Doe</p>
        <div class="stars ">
            <i class="bc-bc-star star_icon"></i>
            <i class="bc-bc-star star_icon"></i>
            <i class="bc-bc-star star_icon"></i>
            <i class="bc-bc-star star_icon"></i>
            <i class="bc-bc-star star_icon"></i>&nbsp;
            <small class="font-weight-bold">(5)</small>
        </div>
    </div>
</div>
</div>
<div class="item">
<div>
    <img src="../images/book_image.svg" alt="logo" class="img-fluid round club-image w-100">
    <div class="card-body slide-content p-0">
        <p class="font-weight-bold book-title" >Out
            Of the Box</p>
            <p class="text-muted " >by John Doe</p>
            <div class="stars">
                <i class="bc-bc-star star_icon"></i>
                <i class="bc-bc-star star_icon"></i>
                <i class="bc-bc-star star_icon"></i>
                <i class="bc-bc-star star_icon"></i>
                <i class="bc-bc-star star_icon"></i>&nbsp;
                <small class="font-weight-bold">(5)</small>
            </div>
        </div>
    </div>
</div>
<div class="item">
    <div>
        <img src="../images/book_image.svg" alt="logo" class="img-fluid round club-image w-100">
        <div class="card-body slide-content p-0">
            <p class="font-weight-bold book-title" >Out
                Of the Box</p>
                <p class="text-muted " >by John Doe</p>
                <div class="stars ">
                    <i class="bc-bc-star star_icon"></i>
                    <i class="bc-bc-star star_icon"></i>
                    <i class="bc-bc-star star_icon"></i>
                    <i class="bc-bc-star star_icon"></i>
                    <i class="bc-bc-star star_icon"></i>&nbsp;
                    <small class="font-weight-bold">(5)</small>
                </div>
            </div>
        </div>
    </div>
    <div class="item">
        <div>
            <img src="../images/book_image.svg" alt="logo" class="img-fluid round club-image w-100">
            <div class="card-body slide-content p-0">
                <p class="font-weight-bold book-title" >Out
                    Of the Box</p>
                    <p class="text-muted " >by John Doe</p>
                    <div class="stars ">
                        <i class="bc-bc-star star_icon"></i>
                        <i class="bc-bc-star star_icon"></i>
                        <i class="bc-bc-star star_icon"></i>
                        <i class="bc-bc-star star_icon"></i>
                        <i class="bc-bc-star star_icon"></i>&nbsp;
                        <small class="font-weight-bold">(5)</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div>
                <img src="../images/book_image.svg" alt="logo" class="img-fluid round club-image w-100">
                <div class="card-body book-content slide-content p-0">
                    <p class="font-weight-bold book-title" >Out
                        Of the Box</p>
                        <p class="text-muted " >by John Doe</p>
                        <div class="stars ">
                            <i class="bc-bc-star star_icon"></i>
                            <i class="bc-bc-star star_icon"></i>
                            <i class="bc-bc-star star_icon"></i>
                            <i class="bc-bc-star star_icon"></i>
                            <i class="bc-bc-star star_icon"></i>&nbsp;
                            <small class="font-weight-bold">(5)</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div>
                    <img src="../images/book_image.svg" alt="logo" class="img-fluid round club-image w-100">
                    <div class="card-body slide-content p-0">
                        <p class="font-weight-bold book-title" >Out
                            Of the Box</p>
                            <p class="text-muted" >by John Doe</p>
                            <div class="stars ">
                                <i class="bc-bc-star star_icon"></i>
                                <i class="bc-bc-star star_icon"></i>
                                <i class="bc-bc-star star_icon"></i>
                                <i class="bc-bc-star star_icon"></i>
                                <i class="bc-bc-star star_icon"></i>&nbsp;
                                <small class="font-weight-bold">(5)</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </section>
    <section class="author-content">
        <p class="font-weight-bold author_heading align-middle">AUTHOR OF THE MONTH</p>
        <!-- <div class="container-fluid"> -->
            <div class="row">
                <div class="col-sm-8 col-md-6 student-of-month">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-6"><img src="../images/avatar4.jpg" class="img-fluid img-height" alt="logo"></div>
                                <div class="col-6">
                                    <div class="row">
                                        <span class="float-left card_content">Book Name:<span class="card_content_data">Out of the Box</span></span>
                                    </div>
                                    <div class="row">
                                        <span class="float-left card_content">Author Name:<span class="card_content_data">JOHN DOE</span></span>
                                    </div>
                                    <div class="row">
                                        <span class="float-left card_content">Description:</span>
                                    </div>
                                    <div class="row">
                                        <span class="card_content_data">Lorem ipsumdolor sit amet consectetur adipisicing elit</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row float-right mt-3">
                                <a href="#" class=" view-more-link" style=" ">View More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </section>
    <section class="reader-content">
        <!-- <div class="container-fluid"> -->
            <div class="card">
                <div class="card-body books">
                    <p class="font-weight-bold float-left title3">
                        Club Reader</p>
                        <button class="btn1 float-right"><a href="#">More</a></button>
                    </div>
                </div>
                <div class="owl-carousel owl-theme">
                    <div class="item club-items">
                        <img src="../images/img_avatar3.png" alt="logo" class="img-fluid club-image">
                        <button class="btn3 literature">Literature</button>
                        <p class="font-weight-bold grp-name" >
                            Group: A
                        </p>
                    </div>
                    <div class="item club-items">
                        <img src="../images/img_avatar3.png" alt="logo" class="img-fluid club-image">
                        <button class="btn3 sci-fi" >Literature</button>
                        <p class="font-weight-bold grp-name" >Group: A</p>
                    </div>
                    <div class="item club-items">
                        <img src="../images/img_avatar3.png" alt="logo" class="img-fluid club-image">
                        <button class="btn3 romance" >Literature</button>
                        <p class="font-weight-bold grp-name">Group: A</p>
                    </div>
                    <div class="item club-items">
                        <img src="../images/img_avatar3.png" alt="logo" class="img-fluid club-image">
                        <button class="btn3 crime" >Literature</button>
                        <p class="font-weight-bold grp-name">Group: A</p>
                    </div>
                    <div class="item club-items">
                        <img src="../images/img_avatar3.png" alt="logo" class="img-fluid club-image">
                        <button class="btn3 sci-fi">Literature</button>
                        <p class="font-weight-bold grp-name">Group: A</p>
                    </div>
                    <div class="item club-items">
                        <img src="../images/img_avatar3.png" alt="logo" class="img-fluid club-image">
                        <button class="btn3 crime" >Literature</button>
                        <p class="font-weight-bold grp-name">Group: A</p>
                    </div>
                </div>
            <!-- </div> -->
        </section>
        <section class="last-content">
            <h6 class="font-weight-bold align-center title5">STUDENT OF THE MONTH</h6>
            <!-- <div class="container-fluid"> -->
                <div class="row">
                    <div class="col-sm-8 col-md-6 student-of-month">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6"><img src="../images/avatar4.jpg" class="img-fluid img-height" alt="logo"></div>
                                    <div class="col-6">
                                        <div class="row">
                                            <span class="float-left card_content">Student Name:<span class="card_content_data">John Smith</span></span>
                                        </div>
                                        <div class="row">
                                            <span class="float-left card_content">Read Books:<span class="card_content_data">145</span></span>
                                        </div>
                                        <div class="row">
                                            <span class="float-left card_content">Favourite Categories:</span>
                                        </div>
                                        <div class="row">
                                            <button class="category_button literature rounded">Literature</button>
                                            <button class="category_button romance rounded">Romance</button>
                                            <button class="category_button youth-age rounded">Youth Age</button>
                                            <button class="category_button crime rounded">Crime</button>
                                            <button class="category_button sci-fi rounded">Sci-Fi</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- </div> -->
        </section>
    </div>
    <?php include "../footer/footer.php";?>
</body>
<script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/jquery.js"></script>
<script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/popper.js"></script>
<script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/bootstrap.min.js"></script>
<script src="../OwlCarousel2/dist/owl.carousel.min.js"></script>
<script src="../js/main.js"></script>
<script src="../header/js/header.js"></script>
</html>
