<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Profile Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://assets.bookchor.xyz/global/vendor/bootstrap-4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat" >
    <link rel="stylesheet" href="../header/css/header.css">
    <link rel="stylesheet" href="../footer/css/footer.css">
    <link rel="stylesheet" href="../assets/bookchor_icons/styles.css">
    <link rel="stylesheet" href="css/profile_page.css">
</head>
<body>
    <?php include "../header/header.php";?>
    <section class="main-content">
        <div class="container">
            <div class="row">
                <div class="grid">
                    <div class="card profile-card">
                        <div class="card-body">
                            <h6 id="title" class="font-weight-bold align-center">My
                                Profile</h6>
                                <img src="../images/img_avatar1.png" class="mx-auto d-block rounded-circle" alt="image">
                            </div>
                        </div>
                        <div class="card student-info">
                            <div class="card-body">
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <td>Name</td>
                                            <td class="font-weight-bold">John Doe</td>
                                        </tr>
                                        <tr>
                                            <td>Class</td>
                                            <td class="font-weight-bold">XIIth Standard</td>
                                        </tr>
                                        <tr>
                                            <td>Roll NO</td>
                                            <td class="font-weight-bold">30</td>
                                        </tr>
                                        <tr>
                                            <td>Admission No</td>
                                            <td class="font-weight-bold">4500</td>
                                        </tr>
                                        <tr>
                                            <td>Current Read</td>
                                            <td class="font-weight-bold">20 Books</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container-fluid">
                <div class="row">
                    <div class="grid2">
                        <div>
                            <p class="font-weight-bold ">Issue Return</p>
                            <br>
                            <div class="card">
                                <div class="card-body group-cards" >
                                    <div class="grid3">
                                        <div class="group-icons">
                                            <img src="../images/Issue_Icon.svg" class="img-fluid float-left" alt="image">
                                        </div>
                                        <div class="group-text">
                                            <p class="float-left">Issue Cycle for the regular book is 7 days</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <p class="font-weight-bold ">Wishlist</p>
                            <br>
                            <div class="card">
                                <div class="card-body group-cards" >
                                    <div class="grid3">
                                        <div class="group-icons">
                                            <img src="../images/Wishlist-heart.svg" class="img-fluid float-left" alt="image">
                                        </div>
                                        <div class="group-text">
                                            <p>Your Personal Redlist</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <p class="font-weight-bold ">Recommendation</p>
                            <br>
                            <div class="card">
                                <div class="card-body group-cards" >
                                    <div class="grid3">
                                        <div class="group-icons">
                                            <img src="../images/Recommended.svg" class="img-fluid float-left" alt="image">
                                        </div>
                                        <div class="group-text">
                                            <p> Books recommended by your Peers</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <p class="font-weight-bold ">E-Books</p>
                            <br>
                            <div class="card">
                                <div class="card-body group-cards" >
                                    <div class="grid3">
                                        <div class="group-icons">
                                            <img src="../images/E-book.svg" class="img-fluid float-left" alt="image">
                                        </div>
                                        <div class="group-text">
                                            <p> Access Ebooks from anywhere</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <p class="font-weight-bold ">Book Club</p>
                            <br>
                            <div class="card">
                                <div class="card-body group-cards">
                                    <div class="grid3">
                                        <div class="group-icons">
                                            <img src="../images/Club_Group.svg" class="img-fluid float-left" alt="image">
                                        </div>
                                        <div class="group-text">
                                            <p>Detail Retaining to the book club</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include "../footer/footer.php";?>
    </body>
    <script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/jquery.js"></script>
    <script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/popper.js"></script>
    <script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/bootstrap.min.js"></script>
    <script src="../header/js/header.js"></script>
    </html>
