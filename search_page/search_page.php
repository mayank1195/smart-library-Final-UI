<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CATEGORIES PAGE</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://assets.bookchor.xyz/global/vendor/bootstrap-4/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/categories_page.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat" >
    <link rel="stylesheet" href="header/css/header.css">
    <link rel="stylesheet" href="footer/css/footer.css">
    <link rel="stylesheet" href="assets/bookchor_icons/styles.css">
</head>
<body>
    <?php include "header/header.php";?>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <h5 class="font-weight-bold page-title pl-4">All Books</h5>
                </div>
                <div class=" col-md-6 col-lg-6 col-xl-6 ">
                    <div class="card test2">
                        <div class="card-body">
                            <div class="grid2">
                                <div class="card-body">
                                    <img src="images/Book Image.svg" alt="logo" class="img-fluid" style="width:100%;">
                                </div>
                                <div class="card-body">
                                    <h6 class="font-weight-bold float-left book-title" >Out of the
                                        Box</h6>
                                        <br>
                                        <small class="text-muted float-left">BY JOHN DOE</small>
                                        <br>
                                        <small>(12 reviews)</small>
                                        <button type="button" class="btn btn-success rating_button pl-0 pt-0 pb-0">5.0<span class="bc-bc-star star_icon"></button>
                                            <br><br>
                                            <small>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa quos eos
                                                quis
                                                est nobis reiciendis officia voluptatum, et eligendi ipsa....</small>
                                                <br><br>
                                                <button id="add-button" class="btn add-to-wishlist pl-1">Add to Wishlist&nbsp;&nbsp;
                                                    <span><i class="bc-bc-heart heart_icon_wishlist"></i></span></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card book-availability">
                                            <div class="card-body " style="padding:15px !important">
                                                <p class="font-weight-bold">Book Availability</p>
                                                <div class="grid4">
                                                    <button class="btn1 font-weight-bold" >SF4</button>
                                                    <button class="btn1 font-weight-bold" >SF4</button>
                                                    <button class="btn1 font-weight-bold" >SF4</button>
                                                    <button class="btn1 font-weight-bold" >SF4</button>
                                                    <button class="btn1 font-weight-bold" >SF4</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card bar-code">
                                            <div class="card-body">
                                                <input type="text" class="form-control" id="usr" placeholder="Unique Bar-Code of the book">
                                            </div>
                                        </div>
                                        <div class="card-body return-date">
                                            <div class="grid3">
                                                <div class="card-body one">
                                                    <button class="btn2 align-center font-weight-bold issue"><a href="#">Issue Book</a></button>
                                                </div>
                                                <div class="card-body two">
                                                    <button class="btn2 align-center font-weight-bold find"><a href="#">Find Book</a></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="card test2">
                                        <div class="card-body">
                                            <div class="grid2">
                                                <div class="card-body">
                                                    <img src="images/Book Image.svg" alt="logo" class="img-fluid" style="width:100%;">
                                                </div>
                                                <div class="card-body">
                                                    <h6 class="font-weight-bold float-left book-title" >Out of the
                                                        Box</h6>
                                                        <br>
                                                        <small class="text-muted float-left">BY JOHN DOE</small>
                                                        <br>
                                                        <small>(12 reviews)</small>
                                                        <button type="button" class="btn btn-success rating_button pl-0 pt-0 pb-0">5.0<span class="bc-bc-star star_icon"></button>
                                                        <br><br>
                                                        <small>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa quos eos
                                                            quis
                                                            est nobis reiciendis officia voluptatum, et eligendi ipsa....</small>
                                                            <br><br>
                                                            <button id="add-button" class="btn add-to-wishlist pl-1">Add to Wishlist&nbsp;&nbsp;
                                                                <span><i class="bc-bc-heart heart_icon_wishlist"></i></span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card book-availability">
                                                        <div class="card-body " style="padding:15px !important">
                                                            <p class="font-weight-bold">Book Availability</p>
                                                            <div class="grid4">
                                                                <button class="btn1 font-weight-bold" >SF4</button>
                                                                <button class="btn1 font-weight-bold" >SF4</button>
                                                                <button class="btn1 font-weight-bold" >SF4</button>
                                                                <button class="btn1 font-weight-bold" >SF4</button>
                                                                <button class="btn1 font-weight-bold" >SF4</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card bar-code">
                                                        <div class="card-body">
                                                            <input type="text" class="form-control" id="usr" placeholder="Unique Bar-Code of the book">
                                                        </div>
                                                    </div>
                                                    <div class="card-body return-date">
                                                        <div class="grid3">
                                                            <div class="card-body one">
                                                                <button class="btn2 align-center font-weight-bold issue"><a href="#">Issue Book</a></button>
                                                            </div>
                                                            <div class="card-body two">
                                                                <button class="btn2 align-center font-weight-bold find"><a href="#">Find Book</a></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class=" col-md-6 col-lg-6 col-xl-6 ">
                                                <div class="card test2">
                                                    <div class="card-body">
                                                        <div class="grid2">
                                                            <div class="card-body">
                                                                <img src="images/Book Image.svg" alt="logo" class="img-fluid" style="width:100%;">
                                                            </div>
                                                            <div class="card-body">
                                                                <h6 class="font-weight-bold float-left book-title" >Out of the
                                                                    Box</h6>
                                                                    <br>
                                                                    <small class="text-muted float-left">BY JOHN DOE</small>
                                                                    <br>
                                                                    <small>(12 reviews)</small>
                                                                    <button type="button" class="btn btn-success rating_button pl-0 pt-0 pb-0">5.0<span class="bc-bc-star star_icon"></button>
                                                                    <br><br>
                                                                    <small>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa quos eos
                                                                        quis
                                                                        est nobis reiciendis officia voluptatum, et eligendi ipsa....</small>
                                                                        <br><br>
                                                                        <button id="add-button" class="btn add-to-wishlist pl-1">Add to Wishlist&nbsp;&nbsp;
                                                                            <span><i class="bc-bc-heart heart_icon_wishlist"></i></span></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="card book-availability">
                                                                    <div class="card-body " style="padding:15px !important">
                                                                        <p class="font-weight-bold">Book Availability</p>
                                                                        <div class="grid4">
                                                                            <button class="btn1 font-weight-bold" >SF4</button>
                                                                            <button class="btn1 font-weight-bold" >SF4</button>
                                                                            <button class="btn1 font-weight-bold" >SF4</button>
                                                                            <button class="btn1 font-weight-bold" >SF4</button>
                                                                            <button class="btn1 font-weight-bold" >SF4</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="card bar-code">
                                                                    <div class="card-body">
                                                                        <input type="text" class="form-control" id="usr" placeholder="Unique Bar-Code of the book">
                                                                    </div>
                                                                </div>
                                                                <div class="card-body return-date">
                                                                    <div class="grid3">
                                                                        <div class="card-body one">
                                                                            <button class="btn2 align-center font-weight-bold issue"><a href="#">Issue Book</a></button>
                                                                        </div>
                                                                        <div class="card-body two">
                                                                            <button class="btn2 align-center font-weight-bold find"><a href="#">Find Book</a></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="card test2">
                                                                <div class="card-body">
                                                                    <div class="grid2">
                                                                        <div class="card-body">
                                                                            <img src="images/Book Image.svg" alt="logo" class="img-fluid" style="width:100%;">
                                                                        </div>
                                                                        <div class="card-body">
                                                                            <h6 class="font-weight-bold float-left book-title" >Out of the
                                                                                Box</h6>
                                                                                <br>
                                                                                <small class="text-muted float-left">BY JOHN DOE</small>
                                                                                <br>
                                                                                <small>(12 reviews)</small>
                                                                                <button type="button" class="btn btn-success rating_button pl-0 pt-0 pb-0">5.0<span class="bc-bc-star star_icon"></button>
                                                                                <br><br>
                                                                                <small>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa quos eos
                                                                                    quis
                                                                                    est nobis reiciendis officia voluptatum, et eligendi ipsa....</small>
                                                                                    <br><br>
                                                                                    <button id="add-button" class="btn add-to-wishlist pl-1">Add to Wishlist&nbsp;&nbsp;
                                                                                        <span><i class="bc-bc-heart heart_icon_wishlist"></i></span></button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="card book-availability">
                                                                                <div class="card-body " style="padding:15px !important">
                                                                                    <p class="font-weight-bold">Book Availability</p>
                                                                                    <div class="grid4">
                                                                                        <button class="btn1 font-weight-bold" >SF4</button>
                                                                                        <button class="btn1 font-weight-bold" >SF4</button>
                                                                                        <button class="btn1 font-weight-bold" >SF4</button>
                                                                                        <button class="btn1 font-weight-bold" >SF4</button>
                                                                                        <button class="btn1 font-weight-bold" >SF4</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="card bar-code">
                                                                                <div class="card-body">
                                                                                    <input type="text" class="form-control" id="usr" placeholder="Unique Bar-Code of the book">
                                                                                </div>
                                                                            </div>
                                                                            <div class="card-body return-date">
                                                                                <div class="grid3">
                                                                                    <div class="card-body one">
                                                                                        <button class="btn2 align-center font-weight-bold issue"><a href="#">Issue Book</a></button>
                                                                                    </div>
                                                                                    <div class="card-body two">
                                                                                        <button class="btn2 align-center font-weight-bold find"><a href="#">Find Book</a></button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <br><br>
                                                                </div>
                                                                <div class="form-row text-center pt-2">
                                                                    <div class="col-6">
                                                                        <button  class=" btn3 float-right navigate_button"><a href="#">Previous Page</a></button>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <button  class=" btn3 float-left navigate_button"><a href="#">Next Page</a></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </section>
                                                        <?php include "footer/footer.php";?>
                                                    </body>
                                                    <script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/jquery.js"></script>
                                                    <script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/popper.js"></script>
                                                    <script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/bootstrap.min.js"></script>
                                                    <script src="js/main.js"></script>
                                                    <script src="header/js/header.js"></script>
                                                    </html>
