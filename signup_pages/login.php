<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://assets.bookchor.xyz/global/vendor/bootstrap-4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat" >
    <link rel="stylesheet" href="css/login.css">
</head>
<body>
    <div class="container-fluid">
        <div class="row ">
            <div class="col-md-6 col-lg-7 col-xl-7">
                <img src="../images/Login-Desktop(new).svg" class="display_img mx-auto img-fluid w-100" alt="logo">
                <h5 class="title">MY SMART LIBRARY</h5>
            </div>
            <div class=" col-md-6 col-lg-5 col-xl-5 ">
                <div class="mobile">
                    <img src="../images/Tab_iMAGE.svg" class="tab_img mx-auto img-fluid" alt="logo" style="width:100%;">
                </div>
                <div class="main-content" style="position:relative;">
                    <div class="card ">
                        <div class="card-body">
                            <div class="cotainer">
                                <div class="mt-3 mb-3 toggle_option">
                                    <ul class="nav nav-pills" role="tablist">
                                        <li class="nav-item ml-0">
                                            <a class="nav-link active" data-toggle="pill" href="#signin_form">Login</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="pill" href="#signup_form">Signup</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content">
                                    <div class="container tab-pane active" id="signin_form">
                                        <form class="form"  name="sign-in" >
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Username</label>
                                                <input type="name" class="form-control input_inline" name="username" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter username...">
                                            </div>
                                            <br>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">Password</label>
                                                <input type="password" class="form-control input_inline" name="password" id="exampleInputPassword1" placeholder="Password">
                                                <a href="#">
                                                    <small id="emailHelp" class="form-text ">FORGET PASSWORD ?</small>
                                                </a>
                                            </div>
                                            <br>
                                            <div class="text-center">
                                                <input type="submit" class="btn" id="login" value="Login">
                                            </div>
                                            <br>
                                        </form>
                                    </div>
                                    <div class="container tab-pane" id="signup_form">
                                        <form class="form" name="sign-up">
                                            <div class="form-group">
                                                <label for="exampleInputusername">Username</label>
                                                <input type="name" class="form-control input_inline" id="exampleInputusername" name="user_name" aria-describedby="emailHelp" placeholder="Enter username...">
                                            </div>
                                            <br />
                                            <div class="form-group">
                                                <input type="image" src="../images/Touch_ID.png" alt="Submit" name="fingerprint" width="48" height="48" id="exampleInputfid" class="fingerprint" accept="image/*">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="align-center" style="position:absolute; right:0; bottom:0;">Powered by Bookchor</p>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/jquery.js"></script>
<script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/popper.js"></script>
<script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/bootstrap.min.js"></script>
<script src="http://assets.bookchor.xyz/global/vendor/jquery_validation_plugin/dist/jquery.validate.min.js"></script>
<script src="js/login.js"></script>
</html>
