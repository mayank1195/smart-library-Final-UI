<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Reset</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://assets.bookchor.xyz/global/vendor/bootstrap-4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat" >
    <link rel="stylesheet" href="css/reset.css">
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class=" col-xs-12 col-lg-7 col-md-6 ">
                <img src="../images/Login-Desktop(new).svg" class="display_img" alt="logo" style="width:100%">
                <h3 class="title">MY SMART LIBRARY</h3>
            </div>
            <div class="col-xs-12 col-lg-5 col-md-6 ">
                <img src="../images/Group.svg" class="tab_img mx-auto d-inline" alt="logo" style="width:100%;">
                <div class="main-content">
                    <div class="card">
                        <div class="card-body">
                            <img src="../images/school_logo.svg" alt="logo">
                            <form class="form">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email ID</label>
                                    <br>
                                    <input type="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                                        placeholder="Enter username...">
                                </div>
                                <small class="text-muted">Enter your Email below to recieve your password reset
                                    instructions</small>
                                <br>
                                <button type="submit" class="btn ">Login</button>
                                <br>
                            </form>
                        </div>
                    </div>
                </div>
                <p>Powered by Bookchor</p>
            </div>
        </div>
    </div>
    <!-- <script src="" async defer></script> -->
</body>
</html>
