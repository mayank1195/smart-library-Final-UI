<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Successful</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://assets.bookchor.xyz/global/vendor/bootstrap-4/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="css/successful.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col">
                <img src="../images/Congra.svg" alt="logo" class="img-fluid image1">
                <h2 class="title align-center">MY SMART LIBRARY</h2>
                <img src="../images/Congra.svg" alt="logo" class="img-fluid image2">
                <div class="card align-center">
                    <div class="card-body">
                        <img src="../images/school_logo.svg" alt="logo" class="img-fluid">
                        <h4 class="display-5 align-center">CONGRATULATIONS</h4>
                        <div class="content align-center">
                            <p class="align-center">Password Changed</p>
                            <p class="align-center">Successfully !!</p>
                        </div>
                    </div>
                </div>
                <p class="align-center font-weight-bold last-tagline">Powered by Bookchor</p>
            </div>
        </div>
    </div>
    <!-- <script src="" async defer></script> -->
</body>
</html>
