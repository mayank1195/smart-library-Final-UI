<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NO_WISHLIST</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://assets.bookchor.xyz/global/vendor/bootstrap-4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat" >
    <link rel="stylesheet" href="../header/css/header.css">
    <link rel="stylesheet" href="../footer/css/footer.css">
    <link rel="stylesheet" href="../assets/bookchor_icons/styles.css">
    <link rel="stylesheet" href="css/no_wishlist.css">
</head>
<body>
    <?php include "../header/header.php";?>
    <section>
        <div class="container-fluid outer page_background">
            <h2 class="align-center font-weight-bold p-2">Wishlist Books</h2>
            <img src="../images/Wishlist_mobile.svg" alt="logo" class="img-fluid w-100">
            <p class="align-center pb-2">Your wishlist is Empty</p>
            <button type="submit" class="button d-block p-2"><a href="#">Continue Search Books</a></button>
        </div>
    </section>
    <?php include "../footer/footer.php";?>
</body>
<script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/jquery.js"></script>
<script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/popper.js"></script>
<script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/bootstrap.min.js"></script>
<script src="../header/js/header.js"></script>
</html>
