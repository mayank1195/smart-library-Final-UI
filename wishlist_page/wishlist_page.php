<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Wishlist Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://assets.bookchor.xyz/global/vendor/bootstrap-4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat" >
    <link rel="stylesheet" href="../header/css/header.css">
    <link rel="stylesheet" href="../footer/css/footer.css">
    <link rel="stylesheet" href="../assets/bookchor_icons/styles.css">
    <link rel="stylesheet" href="css/wishlist_page.css">
</head>
<body>
    <?php include "../header/header.php";?>
    <section>
        <div class="container-fluid outer">
            <div class="row">
                <!-- <div class="col"> -->
                    <h5 class="font-weight-bold pl-4 mt-3">Wishlist Books</h5>
                    <div class="grid">
                        <div class="card">
                            <div class="card-body">
                                <div class="grid2">
                                    <div class="card-body">
                                        <img src="../images/book_image.svg" alt="logo" class="img-fluid" style="width:100%;">
                                    </div>
                                    <div class="card-body">
                                        <p class="font-weight-bold float-left book-title" >Out of the
                                            Box</p>
                                            <br>
                                            <small class="text-muted float-left">BY JOHN DOE</small>
                                            <br>
                                            <small class="font-weight-bold">(12 reviews)</small>
                                            <button type="button" class="btn btn-success rating_button pl-0 pt-0 pb-0">5.0<span class="bc-bc-star star_icon"></button>
                                                <br>
                                                <small style="text-align:justify">
                                                    Lorem ipsum dolor sit amet
                                                    consectetur
                                                    adipisicing elit. Culpa quos eos quis
                                                    est nobis reiciendis officia voluptatum, et eligendi ipsa....</small>
                                                    <br><br>
                                                    <div class="row">
                                                        <button class="category_button literature rounded">Literature</button>
                                                        <button class="category_button romance rounded">Romance</button>
                                                        <button class="category_button crime rounded">Crime</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body return-date">
                                            <div class="grid3">
                                                <div class="card-body one">
                                                    <p class="align-center font-weight-bold"><a href="#">Issue Book</a></p>
                                                </div>
                                                <div class="card-body two">
                                                    <p class="align-center font-weight-bold"><a href="#">Find Book</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="grid2">
                                                <div class="card-body">
                                                    <img src="../images/book_image.svg" alt="logo" class="img-fluid" style="width:100%;">
                                                </div>
                                                <div class="card-body">
                                                    <p class="font-weight-bold float-left book-title" >Out of the
                                                        Box</p>
                                                        <br>
                                                        <small class="text-muted float-left">BY JOHN DOE</small>
                                                        <br>
                                                        <small class="font-weight-bold">(14 Reviews)</small>
                                                        <button type="button" class="btn btn-success rating_button pl-0 pt-0 pb-0">5.0<span class="bc-bc-star star_icon"></button>
                                                            <br>
                                                            <small>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa quos eos
                                                                quis
                                                                est nobis reiciendis officia voluptatum, et eligendi ipsa....</small>
                                                                <br><br>
                                                                <div class="row">
                                                                    <button class="category_button literature rounded">Literature</button>
                                                                    <button class="category_button romance rounded">Romance</button>
                                                                    <button class="category_button crime rounded">Crime</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-body return-date">
                                                        <div class="grid3">
                                                            <div class="card-body one">
                                                                <p class="align-center font-weight-bold"><a href="#">Issue Book</a></p>
                                                            </div>
                                                            <div class="card-body two">
                                                                <p class="align-center font-weight-bold"><a href="#">Find Book</a></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="grid2">
                                                            <div class="card-body">
                                                                <img src="../images/book_image.svg" alt="logo" class="img-fluid" style="width:100%;">
                                                            </div>
                                                            <div class="card-body">
                                                                <p class="font-weight-bold float-left book-title" >Out of the
                                                                    Box</p>
                                                                    <br>
                                                                    <small class="text-muted float-left">BY JOHN DOE</small>
                                                                    <br>
                                                                    <small class="font-weight-bold">(14 Reviews)</small>
                                                                    <button type="button" class="btn btn-success rating_button pl-0 pt-0 pb-0">5.0<span class="bc-bc-star star_icon"></button>
                                                                        <br>
                                                                        <small>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa quos eos
                                                                            quis
                                                                            est nobis reiciendis officia voluptatum, et eligendi ipsa....</small>
                                                                            <br><br>
                                                                            <div class="row">
                                                                                <button class="category_button literature rounded">Literature</button>
                                                                                <button class="category_button romance rounded">Romance</button>
                                                                                <button class="category_button crime rounded">Crime</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="card-body return-date">
                                                                    <div class="grid3">
                                                                        <div class="card-body one">
                                                                            <p class="align-center font-weight-bold"><a href="#">Issue Book</a></p>
                                                                        </div>
                                                                        <div class="card-body two">
                                                                            <p class="align-center font-weight-bold"><a href="#">Find Book</a></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="grid2">
                                                                        <div class="card-body">
                                                                            <img src="../images/book_image.svg" alt="logo" class="img-fluid" style="width:100%;">
                                                                        </div>
                                                                        <div class="card-body">
                                                                            <p class="font-weight-bold float-left book-title" >Out of the
                                                                                Box</p>
                                                                                <br>
                                                                                <small class="text-muted float-left">BY JOHN DOE</small>
                                                                                <br>
                                                                                <small class="font-weight-bold">(14 Reviews)</small>
                                                                                <button type="button" class="btn btn-success rating_button pl-0 pt-0 pb-0">5.0<span class="bc-bc-star star_icon"></button>
                                                                                    <br>
                                                                                    <small>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa quos eos
                                                                                        quis
                                                                                        est nobis reiciendis officia voluptatum, et eligendi ipsa....</small>
                                                                                        <br><br>
                                                                                        <div class="row">
                                                                                            <button class="category_button literature rounded">Literature</button>
                                                                                            <button class="category_button romance rounded">Romance</button>
                                                                                            <button class="category_button crime rounded">Crime</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="card-body return-date">
                                                                                <div class="grid3">
                                                                                    <div class="card-body one">
                                                                                        <p class="align-center font-weight-bold"><a href="#">Issue Book</a></p>
                                                                                    </div>
                                                                                    <div class="card-body two">
                                                                                        <p class="align-center font-weight-bold"><a href="#">Find Book</a></p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="card">
                                                                            <div class="card-body">
                                                                                <div class="grid2">
                                                                                    <div class="card-body">
                                                                                        <img src="../images/book_image.svg" alt="logo" class="img-fluid" style="width:100%;">
                                                                                    </div>
                                                                                    <div class="card-body">
                                                                                        <p class="font-weight-bold float-left book-title" >Out of the
                                                                                            Box</p>
                                                                                            <br>
                                                                                            <small class="text-muted float-left">BY JOHN DOE</small>
                                                                                            <br>
                                                                                            <small class="font-weight-bold">(14 Reviews)</small>
                                                                                            <button type="button" class="btn btn-success rating_button pl-0 pt-0 pb-0">5.0<span class="bc-bc-star star_icon"></button>
                                                                                                <br>
                                                                                                <small>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa quos eos
                                                                                                    quis
                                                                                                    est nobis reiciendis officia voluptatum, et eligendi ipsa....</small>
                                                                                                    <br><br>
                                                                                                    <div class="row">
                                                                                                        <button class="category_button literature rounded">Literature</button>
                                                                                                        <button class="category_button romance rounded">Romance</button>
                                                                                                        <button class="category_button crime rounded">Crime</button>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="card-body return-date">
                                                                                            <div class="grid3">
                                                                                                <div class="card-body one">
                                                                                                    <p class="align-center font-weight-bold"><a href="#">Issue Book</a></p>
                                                                                                </div>
                                                                                                <div class="card-body two">
                                                                                                    <p class="align-center font-weight-bold"><a href="#">Find Book</a></p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="card">
                                                                                        <div class="card-body">
                                                                                            <div class="grid2">
                                                                                                <div class="card-body">
                                                                                                    <img src="../images/book_image.svg" alt="logo" class="img-fluid" style="width:100%;">
                                                                                                </div>
                                                                                                <div class="card-body">
                                                                                                    <p class="font-weight-bold float-left book-title">Out of the
                                                                                                        Box</p>
                                                                                                        <br>
                                                                                                        <small class="text-muted float-left">BY JOHN DOE</small>
                                                                                                        <br>
                                                                                                        <small class="font-weight-bold">(14 Reviews)</small>
                                                                                                        <button type="button" class="btn btn-success rating_button pl-0 pt-0 pb-0">5.0<span class="bc-bc-star star_icon"></button>
                                                                                                            <br>
                                                                                                            <small>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa quos eos
                                                                                                                quis
                                                                                                                est nobis reiciendis officia voluptatum, et eligendi ipsa....</small>
                                                                                                                <br><br>
                                                                                                                <div class="row">
                                                                                                                    <button class="category_button literature rounded">Literature</button>
                                                                                                                    <button class="category_button romance rounded">Romance</button>
                                                                                                                    <button class="category_button crime rounded">Crime</button>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="card-body return-date">
                                                                                                        <div class="grid3">
                                                                                                            <div class="card-body one">
                                                                                                                <p class="align-center font-weight-bold"><a href="#">Issue Book</a></p>
                                                                                                            </div>
                                                                                                            <div class="card-body two">
                                                                                                                <p class="align-center font-weight-bold"><a href="#">Find Book</a></p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="card">
                                                                                                    <div class="card-body">
                                                                                                        <div class="grid2">
                                                                                                            <div class="card-body">
                                                                                                                <img src="../images/book_image.svg" alt="logo" class="img-fluid" style="width:100%;">
                                                                                                            </div>
                                                                                                            <div class="card-body">
                                                                                                                <p class="font-weight-bold float-left book-title" >Out of the
                                                                                                                    Box</p>
                                                                                                                    <br>
                                                                                                                    <small class="text-muted float-left">BY JOHN DOE</small>
                                                                                                                    <br>
                                                                                                                    <small class="font-weight-bold">(14 Reviews)</small>
                                                                                                                    <button type="button" class="btn btn-success rating_button pl-0 pt-0 pb-0">5.0<span class="bc-bc-star star_icon"></button>
                                                                                                                        <br>
                                                                                                                        <small>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa quos eos
                                                                                                                            quis
                                                                                                                            est nobis reiciendis officia voluptatum, et eligendi ipsa....</small>
                                                                                                                            <br><br>
                                                                                                                            <div class="row">
                                                                                                                                <button class="category_button literature rounded">Literature</button>
                                                                                                                                <button class="category_button romance rounded">Romance</button>
                                                                                                                                <button class="category_button crime rounded">Crime</button>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="card-body return-date">
                                                                                                                    <div class="grid3">
                                                                                                                        <div class="card-body one">
                                                                                                                            <p class="align-center font-weight-bold"><a href="#">Issue Book</a></p>
                                                                                                                        </div>
                                                                                                                        <div class="card-body two">
                                                                                                                            <p class="align-center font-weight-bold"><a href="#">Find Book</a></p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    <!-- </div> -->
                                                                                                </div>
                                                                                            </div>
                                                                                        </section>
                                                                                        <?php include "../footer/footer.php";?>
                                                                                    </body>
                                                                                    <script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/jquery.js"></script>
                                                                                    <script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/popper.js"></script>
                                                                                    <script src="http://assets.bookchor.xyz/global/vendor/bootstrap-4/js/bootstrap.min.js"></script>
                                                                                    <script src="../header/js/header.js"></script>
                                                                                    </html>
